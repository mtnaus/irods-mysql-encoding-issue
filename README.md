# README #

Short Jargon Java project that reproduces issue around not being able to update metadata properties containing accentuated characters (e.g. éèê) when using iRODS with MySql, and the MySql server/tables are configured with a Latin1 character set (which is the default).

See discussion at https://groups.google.com/forum/#!searchin/irod-chat/heus$20mysql%7Csort:relevance/irod-chat/Jn3e7A-WRRo/qOtlZxR4EQAJ