package us.mtna.irods;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.CatalogSQLException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.domain.Collection;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.query.MetaDataAndDomainData;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * A simple application that illustrates issue with accentuated characters when 
 * 
 * @author pascal.heus@mtna.us
 *
 */
public class App 
{
	static final Logger logger = LogManager.getLogger("App");
	static AbstractApplicationContext ctx;

	static IRODSFileSystem irodsFileSystem;
	static IRODSAccount irodsAccount;
	static IRODSAccessObjectFactory irodsAccessObjectFactory;
	static IRODSFileFactory irodsFileFactory;
	static AvuData avu;
	
	public static void main( String[] args ) throws JargonException, JargonQueryException
	{
		// init
		ctx = new ClassPathXmlApplicationContext(new String []{"applicationContext.xml"});
		ctx.registerShutdownHook();
		
		irodsFileSystem = (IRODSFileSystem) ctx.getBean("irodsFileSystem");
		irodsAccount = (IRODSAccount) ctx.getBean("irodsAccount");
		irodsAccessObjectFactory = (IRODSAccessObjectFactory) ctx.getBean("irodsAccessObjectFactory");
		irodsFileFactory = irodsFileSystem.getIRODSFileFactory(irodsAccount);
		
		// Get root
		IRODSFile home = irodsFileFactory.instanceIRODSFileUserHomeDir(irodsAccount.getUserName());
		IRODSFile testDir = irodsFileFactory.instanceIRODSFile(home.getAbsolutePath(), "testcoll");
		
		// Clean test directory
		if(testDir.exists()) {
			logger.info("Deleting test directory "+testDir.getAbsolutePath());
			testDir.deleteWithForceOption();
		}
		
		// Create test directory
		logger.info("Creating test directory "+testDir.getAbsolutePath());
		testDir.mkdirs();
		
 		// Get test collection AO
		CollectionAO collectionAO=irodsAccessObjectFactory.getCollectionAO(irodsAccount);
		Collection testCollection = collectionAO.findByAbsolutePath(testDir.getAbsolutePath());
		
		// Add a simple property
		// --> This works  
		logger.info("\nAdding simple property");
		collectionAO.addAVUMetadata(testCollection.getAbsolutePath(),
				new AvuData("testprop1","abcde",""));
		dumpCollectionAvu(collectionAO,testCollection.getAbsolutePath());

		// Add an accentuated property
		// --> This works  
		logger.info("\nAdding accentuated property");
		collectionAO.addAVUMetadata(testCollection.getAbsolutePath(),
				new AvuData("testprop2","áàâ",""));
		// --> The value it the MySql r_meta_main table is áàâ  
		// --> But the returned value is ��� instead of áàâ
		// --> Using the imeta command line also return ???
		dumpCollectionAvu(collectionAO,testCollection.getAbsolutePath());
		
		// Modify the accentuated property
		// --> This works  
		logger.info("\nModifying accentuated property");
		collectionAO.modifyAVUMetadata(testCollection.getAbsolutePath(),
				new AvuData("testprop2","áàâ",""),
				new AvuData("testprop2","éèê",""));
		dumpCollectionAvu(collectionAO,testCollection.getAbsolutePath());
		
		// Modify accentuated property using GivenAttributeAndUnit
		// --> This fails as the delete SQL query is binding/comparing with a value of ���
		// This result in a SQL error:
		// Illegal mix of collations (latin1_general_cs,IMPLICIT) and (utf8_general_ci,COERCIBLE) for operation '='
		logger.info("\nModifying accentuated property BasedOnGivenAttributeAndUnit");
		try {
			collectionAO.modifyAvuValueBasedOnGivenAttributeAndUnit(testCollection.getAbsolutePath(),
					new AvuData("testprop2","úùû",""));
		}
		catch(CatalogSQLException e) {
			logger.error("ERROR: "+e.getMessage());
		}
		dumpCollectionAvu(collectionAO,testCollection.getAbsolutePath());

		// Delete accentuated property 
		// --> This works 
		logger.info("\nDeleting accentuated property");
		collectionAO.deleteAVUMetadata(testCollection.getAbsolutePath(),
				new AvuData("testprop2","éèê",""));
		dumpCollectionAvu(collectionAO,testCollection.getAbsolutePath());
		
		
		// The end
		irodsAccessObjectFactory.closeSession();
		irodsFileSystem.close();

	}
	
	//
	// HELPERS
	//
	public static void dumpCollectionAvu(CollectionAO collectionAO, String path) throws JargonException, JargonQueryException {
		List<MetaDataAndDomainData> values = collectionAO.findMetadataValuesForCollection(path);
		dumpAVU(values);
	}
	
	public static void dumpAVU(List<MetaDataAndDomainData> values) {
		for(MetaDataAndDomainData value : values) {
			logger.info(value.getDomainObjectUniqueName()+": "+value.getAvuAttribute()+"="+value.getAvuValue());
		}
	}
}
