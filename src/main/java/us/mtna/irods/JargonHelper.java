package us.mtna.irods;

import java.util.List;

import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.DuplicateDataException;
import org.irods.jargon.core.exception.FileNotFoundException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.domain.Collection;
import org.irods.jargon.core.pub.domain.DataObject;
import org.irods.jargon.core.pub.domain.IRODSDomainObject;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.query.MetaDataAndDomainData;

/**
 * Helper class to interact with iRODS Jargon
 *  
 * @author pascal.heus@mtna.us
 *
 */
public class JargonHelper {

	private IRODSAccount irodsAccount;
	private IRODSAccessObjectFactory irodsAccessObjectFactory;
	
	//===============================================================
	// CONSTRUCTORS
	//===============================================================
	public JargonHelper(IRODSAccount irodsAccount, IRODSAccessObjectFactory irodsAccessObjectFactory) {
		this.irodsAccount=irodsAccount;
		this.irodsAccessObjectFactory=irodsAccessObjectFactory;
	}
	
	//===============================================================
	// METHODS
	//===============================================================
	
	/**
	 * Adds an attribute/value pair to a DataObject or Collection
	 * 
	 * @param object
	 * @param attribute
	 * @param value
	 * @throws FileNotFoundException
	 * @throws DuplicateDataException
	 * @throws JargonException
	 */
	public void addAVU(IRODSDomainObject object, String attribute, String value) 
			throws FileNotFoundException, DuplicateDataException, JargonException {
		AvuData avu = new AvuData(attribute,value,"");
		if(object instanceof DataObject) {
			DataObjectAO dataObjectAO = irodsAccessObjectFactory.getDataObjectAO(irodsAccount);
			dataObjectAO.addAVUMetadata(((DataObject) object).getAbsolutePath(), avu);
 		}
		else if(object instanceof Collection) {
			CollectionAO collectionAO = irodsAccessObjectFactory.getCollectionAO(irodsAccount);
			collectionAO.addAVUMetadata(((Collection) object).getAbsolutePath(), avu);
		}
	}
	
	/**
	 * Modifies an attribute/value pair to a DataObject or Collection
	 * 
	 * @param object
	 * @param attribute
	 * @param oldValue
	 * @param newValue
	 * @throws FileNotFoundException
	 * @throws DuplicateDataException
	 * @throws JargonException
	 */
	public void modifyAVU(IRODSDomainObject object, String attribute, String oldValue, String newValue) 
			throws FileNotFoundException, DuplicateDataException, JargonException {
		AvuData oldAvu = new AvuData(attribute,oldValue,"");
		AvuData newAvu = new AvuData(attribute,newValue,"");
		if(object instanceof DataObject) {
			DataObjectAO dataObjectAO = irodsAccessObjectFactory.getDataObjectAO(irodsAccount);
			dataObjectAO.modifyAVUMetadata(((DataObject) object).getAbsolutePath(), oldAvu,newAvu);
 		}
		else if(object instanceof Collection) {
			CollectionAO collectionAO = irodsAccessObjectFactory.getCollectionAO(irodsAccount);
			collectionAO.modifyAVUMetadata(((Collection) object).getAbsolutePath(), oldAvu,newAvu);
		}
	}

	/**
	 * Deletes an attribute/value pair to a DataObject or Collection
	 * 
	 * @param object
	 * @param attribute
	 * @param value
	 * @throws FileNotFoundException
	 * @throws DuplicateDataException
	 * @throws JargonException
	 */
	public void deleteAVU(IRODSDomainObject object, String attribute, String value) 
			throws FileNotFoundException, DuplicateDataException, JargonException {
		AvuData avu = new AvuData(attribute,value,"");
		if(object instanceof DataObject) {
			DataObjectAO dataObjectAO = irodsAccessObjectFactory.getDataObjectAO(irodsAccount);
			dataObjectAO.deleteAVUMetadata(((DataObject) object).getAbsolutePath(), avu);
 		}
		else if(object instanceof Collection) {
			CollectionAO collectionAO = irodsAccessObjectFactory.getCollectionAO(irodsAccount);
			collectionAO.deleteAVUMetadata(((Collection) object).getAbsolutePath(), avu);
		}
	}
	
	/**
	 * Returns the list of all AVUs for a DataObject or Collection 
	 * 
	 * @param object
	 * @return
	 * @throws FileNotFoundException
	 * @throws JargonException
	 * @throws JargonQueryException
	 */
	public List<MetaDataAndDomainData> getAVUList(IRODSDomainObject object) throws FileNotFoundException, JargonException, JargonQueryException {
		List<MetaDataAndDomainData> values=null;
		if(object instanceof DataObject) {
			DataObjectAO dataObjectAO = irodsAccessObjectFactory.getDataObjectAO(irodsAccount);
			values = dataObjectAO.findMetadataValuesForDataObject(((DataObject) object).getAbsolutePath());
 		}
		else if(object instanceof Collection) {
			CollectionAO collectionAO = irodsAccessObjectFactory.getCollectionAO(irodsAccount);
			values = collectionAO.findMetadataValuesForCollection(((Collection) object).getAbsolutePath());
		}
		return values;
	}
	
	//===============================================================
	// GETTERS/SETTERS
	//===============================================================
	public IRODSAccount getIrodsAccount() {
		return irodsAccount;
	}
	public void setIrodsAccount(IRODSAccount irodsAccount) {
		this.irodsAccount = irodsAccount;
	}
	
	public IRODSAccessObjectFactory getIrodsAccessObjectFactory() {
		return irodsAccessObjectFactory;
	}
	public void setIrodsAccessObjectFactory(IRODSAccessObjectFactory irodsAccessObjectFactory) {
		this.irodsAccessObjectFactory = irodsAccessObjectFactory;
	}
	
}
